let gulp, minify, uglify, concat, changeFile;

gulp            = require('gulp');
minify          = require('gulp-minify');
concat          = require('gulp-concat');
changeFile    = require( 'gulp-change-file-content' );


gulp.task( 'minify', () => {
    return gulp.src( './scripts/**/*.js' )
        .pipe( minify({
            ext: {
                min:        '.js'
            },
            noSource:       true
        }) )
        // .pipe( gulp.dest( './scripts/_minify/' ) )
        .pipe( uglify() )
        .pipe( concat( 'custom.js' ) )
        .pipe( changeFile( ( content ) => {
            const start = 'window.onload=()=>{\n';
            const end = '\n}';
            return `${ start }${ content }${ end }`;
        } ) )
        .pipe( gulp.dest( './assets/js/' ) )
} )

// Need to export variables from config file