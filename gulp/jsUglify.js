let gulp, babel;

gulp = require('gulp');
babel = require('gulp-babel');

gulp.task( 'uglify', () => {
    return gulp.src( './scripts/_minify/*.js' )
        .pipe( babel({
            presets:        [ "@babel/preset-env" ]
        }) )
        // .pipe( uglify() )
        // .pipe( concat( 'script.js' ) )
        // .pipe( gulp.dest( '../assets/js/' ) )
} )