let gulp, browserSync, config;

gulp            = require('gulp');
browserSync     = require('browser-sync').create();

config          = require('./config');

gulp.task( 'sync', () => {
    browserSync.init({
        proxy:              config.projectURL,
        ghostMode:          config.ghostMode,
        open:               config.browserAutoOpen,
        injectChanges:      config.injectChanges,
        watchEvents:        config.watchEvents,
        notify:             config.notify
    })
})
