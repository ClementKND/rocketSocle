let notify, beep, config;

notify      = require('gulp-notify');
beep        = require('beepbeep')
config      = require('./config');

export const errorHandler = ( err ) => {
    notify.onError({
        title:          "❌ - ERROR !",
        message:        "<%= error.message %> " + err,
        icon:           config.notifIcon
    })
    beep();
}