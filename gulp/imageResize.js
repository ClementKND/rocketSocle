let gulp, rename, imgResize, notify, config;

gulp            = require('gulp');
rename          = require('gulp-rename');
imgResize       = require('gulp-image-resize');
notify          = require('gulp-notify');

config          = require('./config');

gulp.task( 'image-resize', ( done ) => {
    config.imagesQuery.forEach( ( size ) => {
        return gulp.src( config.imageDirFiles )
            .pipe( imgResize({
                width: size,
                height: size,
                upscale: false
            }) )
            .pipe( rename( ( path ) => {
                path.basename = `${path.basename}@${size}w`;
            } ))
            .pipe( gulp.dest( config.imageDest ) )
            .on( 'error', notify.onError( ( error ) => {
                return error.message;
            }) )
    })
    done();
} )